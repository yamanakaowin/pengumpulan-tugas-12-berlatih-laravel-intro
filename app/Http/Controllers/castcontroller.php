<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class castcontroller extends Controller
{
    public function create(){
        return view ('cast.create');
    }

    public function store(){
        $request->validate([
            'nama' => 'required',
            'deskripsi' => 'required',
        ]);

        DB::table('cast')->insert(
            [
                'nama' => $request['nama' ],
                'bio' => $request['bio']
            ]
        );

        return redirect('/cast/create');
    }
}

