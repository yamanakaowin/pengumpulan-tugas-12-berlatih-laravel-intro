@extends('layout.master')

@section('judul')
Buat Account Baru!
@endsection

@section('content')

        <h2>Sign Up Form</h2>

        <form action="/welcome" method="post">
            @csrf
        <label for="fname">First name:</label><br><br>
        <input type="text" id="fname" name="fname"
        value=""><br><br>

        <label for="lname">Last name:</label><br><br>
        <input type="text" id="lname" name="lname"
        value="">
        

<p>Gender:</p>

    <input type="radio" id="male"
    name="fav_language" value="male">
    <label for="male">Male</label><br>
    <input type="radio" id="female"
    name="fav_language" value="female">
    <label for="female">Female</label><br>
    <input type="radio" id="other"
    name="fav_language" value="other">
    <label for="other">Other</label><br>

<p>Nationality:</p>

<label for="nationality"></label>
<select name="nationality" id="nationality">
    <option value="Indonesia">Indonesia</option>
    <option value="Amerika">Amerika</option>
    <option value="Inggris">Inggris</option>
</select>

<p>Language Spoken:</p>

    <input type="checkbox" id="vehicle1"
    name="vehicle1" value="ina">
    <label for="vehicle1">Bahasa Indonesia</label><br>
    <input type="checkbox" id="vehicle2"
    name="vehicle2" value="us">
    <label for="vehicle2">English</label><br>
    <input type="checkbox" id="vehicle3"
    name="vehicle3" value="oth">
    <label for="vehicle3">Other</label>


<p>Bio:</p>

<textarea name="message" rows="10"
cols="30"></textarea><br>
<input type="submit"
value="Sign Up">
</form>

@endsection