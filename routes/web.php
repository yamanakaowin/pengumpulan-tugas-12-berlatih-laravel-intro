<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Indexcontroller@index');
Route::get('/register','AuthController@register');
Route::post('/welcome','AuthController@kirim');

/*
Route::get('/master',function()
{
    return view('layout.master');
});
*/

//CRUD Cast
Route::get('/cast/create','castcontroller@create');
Route::post('/cast','castcontroller@store');


// tugas table

Route::get('/table/table',function(){
    return view('table.table');
});

Route::get('/table/data-tables',function(){
    return view('table.data-tables');
});

/*
Route::get('/welcome', function () {
    return view('welcome');
});

Route::get('/', function(){
    return view('halaman.index');
});

Route::get('/register',function (){
    return view('halaman.form');
});

Route::get('/welcome',function (){
    return view('halaman.welcome');
});
*/